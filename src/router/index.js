import Vue from 'vue';
import Router from 'vue-router';
import $http from 'vue-resource';

import Hello from '@/components/Hello';

Vue.use(Router);
Vue.use($http);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello,
    },
  ],
});
